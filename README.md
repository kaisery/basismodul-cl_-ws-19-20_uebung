# Basismodul CL, Übung WS 19/20

*Repository der Übung zum Computerlinguistik Masterkurs P1 "Basismodul Computerlinguistik"*

## Inhalt

In diesem Repository finden sich nach Sitzung sortiert die Materialien und Foliensätze der Übung.

Die Materialien von Prof. Schulz befinden sich [hier](https://www.cis.uni-muenchen.de/people/Schulz/pw/).

## Software

- Die Programmieraufgaben werden in [Python3 (3.6+)](https://www.python.org/) gestellt.
- Die Python-Dokumentation findet sich [hier](https://docs.python.org/3.6/).
- [Jupyter-Notebooks](https://jupyter.org/) stellen die Basis für die Folien dar, die in der Übung verwendet werden und in diesem Repository verfügbar gemacht sind.
- Um die Notebooks als Präsentation anzuzeigen wird [RISE](https://rise.readthedocs.io/en/maint-5.5/) verwendet. RISE wird **nicht** benötigt, um die Notebooks zu benutzen.

## Hausaufgaben

- Die Hausaufgaben sind freiwillig und werden nicht benotet, bei Einsenden aber korrigiert.
- Hausaufgaben können als Jupyter-Notebook oder als .py-Datei(en) per Email abgegeben werden an: Y.Kaiser@campus.lmu.de

## Abschlussprojekt

- Die Beschreibung des Abschlussprojektes kann in deutsch und english [hier](https://gitlab2.cip.ifi.lmu.de/kaisery/basismodul-cl_-ws-19-20_uebung/tree/master/09__12_12_19) gefunden werden.